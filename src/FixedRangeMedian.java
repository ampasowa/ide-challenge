/*
 * Used to compute a running Median using a variation of Counting Sort.
 *
 * Useful for rolling medians calculations when the range of numbers is fixed.
 * It makes use of a fixed array of buckets.
 *
 * @author Nii
 */
public class FixedRangeMedian {
    private final int[] arr;
    private final int buckets;
    private long size;

    public FixedRangeMedian(int buckets) {
        arr = new int[buckets];
        this.buckets = buckets;
    }

    /**
     * Adds a new integer to the running median
     * @param n  integer to be added to the median
     */
    public void add(int n) {
        if (n > buckets) return; // Silently failing ok for the moment
        arr[n]++;
        size++;
    }

    /**
     * Returns the current median of all numbers added
     * @return
     */
    public double getMedian() {
        int current = 0;
        int currentSum = 0;
        boolean odd = (size % 2 == 1) ? true : false;
        long targetSum = size / 2 + (odd ? 1 : 0);

        while (currentSum + arr[current] < targetSum) {
            currentSum += arr[current];
            current++;
        }

        if (odd) {
            return current;
        } else {
            int next = current + 1;
            while (arr[next] == 0) next++;
            return (current + next) / 2.0;
        }
    }
}

