/*
 * TweetStats - Main driver class for Insight Coding Challenge
 *
 * Reads all files in an input folder and writes out two textfiles
 * summarizing features calculated.
 *
 * @author Nii
 */
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class TweetStats {
    private static final String INPUT_FOLDER = "tweet_input";
    private static final String FT_1_OUTPUT = "tweet_output/ft1.txt";
    private static final String FT_2_OUTPUT = "tweet_output/ft2.txt";

    // Pre-compile whitespace regex to speedup splitting later
    private static final Pattern whitespacePattern = Pattern.compile("\\s+");
    private static final HashMap<String, Integer> wordCounts = new HashMap<>();
    private static final FixedRangeMedian median = new FixedRangeMedian(70);

    public static void main(String[] args) throws FileNotFoundException,
           IOException {
               List<File> fileList = getFileList(INPUT_FOLDER);

               FileWriter fw = new FileWriter(FT_2_OUTPUT);
               BufferedWriter bw = new BufferedWriter(fw);
               PrintWriter medianWriter = new PrintWriter(bw);

               for (File inputFile : fileList) {
                   System.out.printf("Processing input file: %s\n",
                           inputFile.getAbsoluteFile());
                   try (
                           FileReader fr = new FileReader(inputFile);
                           BufferedReader br = new BufferedReader(fr);
                       ) {
                       String line, tokens[];
                       while ((line = br.readLine()) != null) {
                           // do not add blank lines
                           if (line.trim().length() == 0) continue;
                           tokens = whitespacePattern.split(line.trim());
                           addWords(tokens);
                           appendMedianUnique(tokens, medianWriter);
                       }
                       }
               }

               sortAndOutputWords();
               medianWriter.close();
               System.out.println("Completed calculating features.");
    }

    /**
     * Returns a list of files in the input folder. Only returns files
     * in the current level. Does not enumerate files recursively.
     *
     * @param folder
     * @return List of files located in input folder
     */
    private static List<File> getFileList(String folder) {
        File inputFolder = new File(folder);
        File[] files = inputFolder.listFiles();
        List<File> fileList = new ArrayList<>(Arrays.asList(files));
        Collections.sort(fileList);
        return fileList;
    }

    /**
     * Adds count of unique words in a line to running median and
     * appends this to output file.
     *
     * @param tokens
     * @param medianWriter
     */
    private static void appendMedianUnique(String[] tokens,
            PrintWriter medianWriter) {
        Set<String> set = new HashSet<>();
        set.addAll(Arrays.asList(tokens));
        median.add(set.size());
        medianWriter.printf("%.02f\n", median.getMedian());
    }

    /**
     * Orders word counts lexicographically and outputs to file
     *
     * @throws IOException
     */
    private static void sortAndOutputWords() throws IOException {
        TreeMap<String, Integer> ordered = new TreeMap<>(wordCounts);
        try (
                FileWriter fw = new FileWriter(FT_1_OUTPUT);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)
            ) {
            for (Map.Entry<String, Integer> entry : ordered.entrySet()) {
                out.printf("%-30s %d\n", entry.getKey(), entry.getValue());
            }
            }
    }

    /**
     * Adds array of words to word counter data structure
     *
     * @param words
     */
    private static void addWords(String[] words) {
        for (String word : words) {
            int count = wordCounts.getOrDefault(word, 0);
            wordCounts.put(word, count + 1);
        }
    }
}

