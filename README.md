# Insight Data Engineering Coding Challenge
Solution to the Insight Data Engineering coding challenge for the September 2015 session. This solution is written in Java, without the use of external libraries.
It has been tested on OS X 10.0.4 using Java 1.8.0_40.

## Usage
From the root of the project, simply execute run.sh as below:

    ./run.sh


## Implementation Notes
WordCount:
- A HashMap was chosen because it made sense to optimize for insertions/lookups which occur very frequently for this use case. The biggest downside of this decision was having to copy all elements into a TreeMap due to the requirement of printing out elements in lexicographical order.
- Use of a [Patricia Trie](https://en.wikipedia.org/wiki/Radix_tree) was explored in order to improve the space efficiency of the solution. When tested with a large dataset, there did not appear to be any advantages over the HashMap, probably due to the fact that the dictionary of words appearing is rather large and ever-expanding due to proper nouns, mispellings, URLs, @usernames etc in tweets. This approach was therefore abandoned in the submitted solution.
- The current solution will not work for datasets larger than the available memory. This can be solved by using an external merge sort.

MedianUniques:
- This was initially implemented using the the technique of two heaps (a Min-Heap and a Max-Heap). While O(log(n)) insertions and O(1) lookups are decent, the space efficiency of the solution O(n) was sub-optimal. This was easily improved with knowldge of the fact that tweets are <= 140 characters. This implies an upper bound on the number of words present in any tweet (70 words - assuming a space after every character). A variation of [Counting Sort](https://en.wikipedia.org/wiki/Counting_sort) was swapped in for the implementation using 70 buckets. This effiently addressed the space issue.

Other:
- Also initially considered the idea of a multithreaded solution, to split the file and let various threads work on parts of it. However, it because apparent when testing with larger files that I/O was the primary bottleneck, so the benefits would only be apparent if the load is distributed over different machines.

